FROM pcjohnson/ubi7_zulu11:latest

WORKDIR /usr/local/bin

COPY target/kubia-ip-hostname-*.jar kubia-ip-hostname.jar
EXPOSE 3000

ENTRYPOINT java -jar kubia-ip-hostname.jar