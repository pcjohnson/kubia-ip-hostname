ZIPKIN_POD_NAME=$(shell kubectl -n istio-system get pod -l app=zipkin -o jsonpath='{.items..metadata.name}')
JAEGER_POD_NAME=$(shell kubectl -n istio-system get pod -l app=jaeger -o jsonpath='{.items..metadata.name}')
SERVICEGRAPH_POD_NAME=$(shell kubectl -n istio-system get pod -l app=servicegraph -o jsonpath='{.items..metadata.name}')
GRAFANA_POD_NAME=$(shell kubectl -n istio-system get pod -l app=grafana -o jsonpath='{.items..metadata.name}')
PROMETHEUS_POD_NAME=$(shell kubectl -n istio-system get pod -l app=prometheus -o jsonpath='{.items..metadata.name}')

get-stuff:
	kubectl get pods && kubectl get svc && kubectl get svc istio-ingressgateway -n istio-system

deploy-app:
	kubectl apply -f ./kubernetes/istio/deployment.yaml
	kubectl apply -f ./kubernetes/istio/services.yaml

ingress:
	kubectl apply -f ./kubernetes/istio/ingress.yaml
egress:
	kubectl apply -f ./kubernetes/istio/egress.yaml
prod:
	kubectl apply -f ./kubernetes/istio/destination-rule.yaml
	kubectl apply -f ./kubernetes/istio/routing-option-1.yaml
retry:
	kubectl apply -f ./kubernetes/istio/routing-option-2.yaml
canary:
	kubectl apply -f ./kubernetes/istio/routing-option-3.yaml

start-monitoring-services:
	@echo "Jaeger: 16686  -  Prometheus: 9090  - Grafana: 3000"
	$(shell kubectl -n istio-system port-forward $(JAEGER_POD_NAME) 16686:16686 & kubectl -n istio-system port-forward $(SERVICEGRAPH_POD_NAME) 8088:8088 & kubectl -n istio-system port-forward $(GRAFANA_POD_NAME) 3000:3000 & kubectl -n istio-system port-forward $(PROMETHEUS_POD_NAME) 9090:9090)


restart-demo:
	-kubectl delete svc --all
	-kubectl delete deployment --all
	-kubectl delete VirtualService --all
	-kubectl delete DestinationRule --all
	-kubectl delete Gateway --all
	-kubectl delete ServiceEntry --all
	
uninstall-istio:
	-kubectl delete service frontend
	-kubectl delete ingress istio-ingress
	-kubectl delete ns istio-system