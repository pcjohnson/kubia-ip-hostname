# Kubernetes & Istio 101
Demo application to show some of the features of Kubernetes & Istio.

## kubia-ip-hostname
This is the same Java app used throughout this demo. Built using Spring Boot, exposing REST endpoints that can be
chained together, returning such deatils as the IP address of the node it is running on, plus it's hostname, and
finally calling World Clock API. 

## Requirements
Docker & Kubernetes running locally. On a Mac see:

[Kubernetes in local the easy way: Docker Desktop (on Mac)](https://medium.com/backbase/kubernetes-in-local-the-easy-way-f8ef2b98be68)

You could also create a cluster in the cloud, AKS, GKE etc. This demo was verified with AKS.

Istio needs to be installed. This demo uses v1.5.2.

Download the latest Istio version, make istioctl executable and move to `/usr/local/bin/`
to make it available in a terminal.

```
curl -L https://istio.io/downloadIstio | sh -
chmod +x ./istio-1.5.2/bin/istioctl
sudo mv ./istio-1.5.2/bin/istioctl /usr/local/bin/istioctl
```

This demo uses the demo profile which has all the services and features enabled.

```
istioctl manifest apply --set profile=demo
```

You can verify Istio has installed by verifying.

```
istioctl verify-install -f <(istioctl manifest generate --set profile=demo)
```        

Finally the demo assumes that sidecars are auto-injected, so set the label accordingly for the `default` namespace.

```
kubectl label namespace default istio-injection=enabled
```

## Helper Services
A [Makefile](Makefile) has been created to help with some simple services.

## Deploy the application
You can of course build the Docker image yourself, you can also use the image defined in the Deployment Kubernetes
yaml file.

```
make deploy-app
```            

### Using Istio
Let's see the Kubernetes resources:

```
make get-stuff
```

You should see something like this:

```
kubectl get pods && kubectl get svc && kubectl get svc istio-ingressgateway -n istio-system
NAME                                 READY   STATUS    RESTARTS   AGE
backend-prod-66c56665-t44b2          2/2     Running   0          5h8m
frontend-prod-7d898b6c55-lzv96       2/2     Running   0          5h8m
middleware-canary-769c747b57-xbjn5   2/2     Running   0          5h8m
middleware-prod-54cb77dfc5-dk5xs     2/2     Running   0          5h8m
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)           AGE
backend      ClusterIP   10.0.136.229   <none>        80/TCP,9464/TCP   5h8m
frontend     ClusterIP   10.0.226.211   <none>        80/TCP,9464/TCP   5h8m
kubernetes   ClusterIP   10.0.0.1       <none>        443/TCP           5h9m
middleware   ClusterIP   10.0.94.93     <none>        80/TCP,9464/TCP   5h8m
NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)                                                                                                                                      AGE
istio-ingressgateway   LoadBalancer   10.0.183.161   20.185.8.126   15020:30553/TCP,80:30111/TCP,443:32647/TCP,15029:30679/TCP,15030:32160/TCP,15031:31873/TCP,15032:30554/TCP,31400:31265/TCP,15443:30983/TCP   6h13m
```

Besides the istio-ingressgateway, Istio is not present. Istio by runs under another namespace.

```
kubectl get svc --namespace istio-system --output wide
kubectl get po --namespace istio-system --output wide
```

As we auto-injecting any Pods that Kubernetes creates in the `default` namespace will automatically get an Istio
sidecar proxy attached to it. This proxy will enforce Istio polices without any action from the application. Of course
the proxy can be added into the Kubernetes YAML manually, again no change to the app, but the Kubernetes
Deployment will be manually patched. This is useful if you want some services to bypass Istio.

### External Access
**Useful if running using a cloud service such as AKS**

None of the demo services have External IPs, for this demo we want Istio to manage all inbound traffic. To do this,
Istio uses an [Ingress Gateways](https://istio.io/docs/tasks/traffic-management/ingress/ingress-control/).

Next, try access the external IP of the `istio-ingressgateway`. You should see an error.

![alt text](images/No_Gateway.png "No gateway to route traffic")        

This is because the gateway hasn't been configured yet, so all traffic will be terminated at the edge of the cluster.

#### Enabling Access

```
make ingress
```

The [ingress.yaml](kubernetes/istio/ingress.yaml) contains two objects.

1. The Gateway will bind to `istio-ingressgateway` that exists inside the cluster.
2. The VirtualService, will allow routing rules to be applied. The demo application uses wildcard (*) character for
the host and only one route rule, all traffic from this gateway to the frontend service.

Visit the external Ip of the `istio-ingressgateway` again. You should see a 200 status code returned.

![alt text](images/Service_Running_200.png "Service Running, code 200")

You should see the frontend service request the middleware service, which requests the backend service, which finally
requests [WorldClockAPI](http://worldclockapi.com/api/json/utc/now)

If however, a 404 is still returned.

![alt text](images/No_Service_Entry.png "No Service Entry to allow traffic to egress")

It's probably due to Istio not allowing traffic out of the cluster by default. This is a great security practice,
as it prevents malicious code from calling home, or your code from talking to unverified 3rd party services.
        
```
make egress
```

The [egress.yaml](kubernetes/istio/egress.yaml) allows http & https traffice to WorldClockAPI host.

Hit the `istio-ingressgateway` again, and you should see a response from WorldClockAPI as above.

## Traffic Routing

Send the request again, and again. You should see the middleware service change. Sometimes, the middleware service calls
the `middleware-canary` and sometimes it calls `middleware-prod`.

![alt text](images/Middleware-Prod.png "Traffic routed through middleware-prod")

![alt text](images/Middleware-Canary.png "Traffic routed through middleware-canary")

This is because there is a single Kubernetes [service](kubernetes/istio/services.yaml) called middleware sending
traffic to two [deployments](kubernetes/istio/deployment.yaml) (called middleware-prod and middleware-canary).
A really powerful feature! This makes Blue-Green deployments and Canary testing possible out of the box. Using
kiali, we can see this visually. From the command line:

```
istioctl dashboard kiali 
```

This will open up kiali into your web browser. Username & Password is `admin`. Navigate to the Graph, setting the
namespace to `default`.

![alt text](images/Kiali_Graph.png "Kiali Graph")

Traffic goes from frontend (prod), then is sent to middleware (prod) or middleware (canary) before going to backend
(prod).

With Istio, you can control where traffic goes using a [VirtualService](https://istio.io/docs/reference/config/istio.networking.v1alpha3/#VirtualService).
For example, you can send 80% of the all traffic that is going to the middleware service to prod and 20% to canary.
In this case, let's route 100% of the traffic to prod, see [routing-option-1](kubernetes/istio/routing-option-1.yaml).

This VirtualService is binding the frontend service to the gateway, then routing to the `prod` destination. The key part
is the `subset`. Subsets are defined in [DestinationRule](https://istio.io/docs/reference/config/istio.networking.v1alpha3/#DestinationRule).

```
spec:
  host: middleware
  trafficPolicy:
    tls:
      mode: ISTIO_MUTUAL
  subsets:
    - name: prod
      labels:
        version: prod
    - name: canary
      labels:
        version: canary
``` 
Extracted from routing-option-1.

First, we are setting a trafficPolicy that ensures that all traffic is encrypted with Mutual TLS. This means that we get
encryption and we can actually restrict which services get access to other services inside the mesh. Even if someone
breaks into your cluster, they can't impersonate another service if you are using mTLS, thus limiting the
damange they can do.

Next, you can see how the subsets are defined. We give each subset a name, and then use Kubernetes lables to select
the pods in each subset. We are creating a prod and canary subet in this case.

Create the DestinationRules and the VirtualServices:

```
make prod
```

**If you kiali dashboard is still running, pause by hitting Control Z,
then sending the job to the backgroud with `bg`.**

Now only the middleware-prod service will be routed.

# Instability

In the real world, services fail all the time. In a microservices world, this means you can have thousands of
downstream failures, and your app needs to be able to handle them. Istio's Service Mesh can be configured to
automatically handle many of these failures so your app doesn't have to!

*Note: Istio has a feature call Fault Injection that can simulate errors. This means you don't need to write bad code
to test if your app can handle failure.*

The app that has been deployed is very stable, but there is a function that will cause the app to randomly
return a http status code of 500!

The code is triggered by a HTTP Header called "fail" which is a number between 0 and 1, with 0 being a 0% chance
of failure, and 1 being 100% chance.

![alt text](images/Postman_Fail.gif "Postman Fail")

A failure rate of 50%, the app fails almost every time! This is because each service forwards headers to the downstream
services, so this is a cascading error!

## Istio Can Fix It!

This is a flaky error, the request will sometimes work and sometimes not work. These are hard to debug because they are
hard to find! If there is a very low percentage chance, then the error will only be triggered maybe one time
in a million. However, that might be an important request that can potentially break everything. There is a simple
fix for this type of flaky error, just retry the request! This only works up to a certain point, but can easily
pave over flaky errors that have low probabilities of occurring.

Normally, code is developed with retry logic in every single one of your microservices for every single
network request. Thankfully, Istio provides this out of the box, so you don't need to modify your code at all!

Let's modify the VirtualServices to add in some retry logic. [routing-option-2](kubernetes/istio/routing-option-2.yaml)
is the updated rule. There is an additional section for each that looks like this:

```
   retries:
     attempts: 3
     perTryTimeout: 2s
     retryOn: 5xx
```

This means that Istio will retry the request three times before giving up, and will wait 2 seconds per retry
(in case the downstream service hangs). Your app just sees it as one request, all the retry complexity is abstracted
away.

Apply the rule:

```
make retry
```

Try again via postman, setting the fail tag in the header with a high value. The overall duration should increase, as
more attempts are made to call the services following a circuit breaker pattern.

![alt text](images/Postman_Pass.gif "Postman Pass")

## Canary

There are two middleware deployments, a `prod` and `canary` version. Currently the rules send all the traffic to `prod`.
This is good, because we don't want normal people accessing the Canary build.

However we do want our development and trusted others to access it. Istio has this covered too.

Routing Rules can have conditional routing based on things like headers, cookies, etc. We could check if a user is
part of the trusted group and set a cookie that lets them access the canary service. A header called "x-dev-user" will
be used and check if the value is "secret".

You can see the new rule in [routing-option-3](kubernetes/istio/routing-option-3.yaml).

Here we are checking if the "x-dev-user" header is set to "secret" and if it is then we send traffic to the canary
subset. Otherwise, traffic gets sent to prod.

```
make canary
```        

Send the proper header, Istio automatically routes you to the right service

![alt text](images/Postman_Canary.gif "Postman Canary")

## Tracing

TBC


