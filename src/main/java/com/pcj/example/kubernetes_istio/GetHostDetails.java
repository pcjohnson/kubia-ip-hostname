package com.pcj.example.kubernetes_istio;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class GetHostDetails {

    private HttpServletRequest request;
    private String url;
    private String serviceName;
    float failureRate = 0f;
    private final Logger logger = LoggerFactory.getLogger(GetHostDetails.class);

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public GetHostDetails() {
        url = Optional.ofNullable(System.getenv("UPSTREAM_URI")).orElse("http://worldclockapi.com/api/json/utc/now");
        serviceName = Optional.ofNullable(System.getenv("SERVICE_NAME")).orElse("kubia-istio-test-v1");
        logger.info("Upstream serviceName {}", serviceName);
        logger.info("Upstream URL {}", url);
    }

    @GetMapping(path = "/")
    public ResponseEntity<String> index() {
        LocalDateTime begin = LocalDateTime.now();

        // Do Bad Things
        failureRate = getFailRate(request.getHeader("fail"));
        boolean problem = createIssues(failureRate);
        logger.info("Create Problem {}", problem);
        if (problem) {
            throw new SimulateError("I'm not well, please restart me!");
        }

        // Build Response
        Response response = new Response();

        String fromAddress = request.getRemoteAddr();
        response.setServiceName(serviceName);
        // Replace pattern-breaking characters
        fromAddress = fromAddress.replaceAll("[\n|\r\t]", "_");
        response.setFromAddress(fromAddress);
        logger.info("Received request from {}", fromAddress);

        InetAddress ip;
        try {
            ip = InetAddress.getLocalHost();
            response.setServiceHostname(ip.getHostName());
            response.setServiceIpAddress(ip.getHostAddress());
        } catch (UnknownHostException e) {
            logger.error("Context: ", e);
        }

        response.setDownstreamUrl(url);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = forwardHeaders();
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        response.setDownstreamResponseStatus(result.getStatusCode().toString());
        response.setDownstreamResponse(new JSONObject(Objects.requireNonNull(result.getBody())));

        LocalDateTime end = LocalDateTime.now();
        response.setTimeDifferenceMS(ChronoUnit.MILLIS.between(begin, end));

        return new ResponseEntity<>(response.toString(), headers, HttpStatus.OK);
    }

    @GetMapping(path = "/health")
    public String healthCheck() {
        failureRate = getFailRate(request.getHeader("fail"));
        boolean problem = createIssues(failureRate);
        logger.info("Health Check - Create Problem {}", problem);
        if (problem) {
            throw new SimulateError("I'm not well, please restart me!");
        } else {
            return "up";
        }
    }

    // Look at the "fail %" header to increase chance of failure
    // Failures cascade, so this number shouldn't be set too high (under 0.3 is good)
    protected boolean createIssues(float failPercent) {
        float absFailPercent = Math.abs(failPercent);
        logger.info("Fail Percent: {}", absFailPercent);
        return Math.random() < absFailPercent;
    }

    protected static float getFailRate(String failPercent) {
        float failRate;
        if (failPercent == null) {
            failRate = 0f;
        } else {
            try {
                failRate = Float.parseFloat(failPercent.trim());
            } catch (NumberFormatException nfe) {
                failRate = 0f;
            }
        }
        return failRate;
    }

    private HttpHeaders forwardHeaders() {
        HttpHeaders newHeaders = new HttpHeaders();
        newHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        newHeaders.setContentType(MediaType.APPLICATION_JSON);

        List<String> appHeaders = new ArrayList<>();
        appHeaders.add("x-request-id");
        appHeaders.add("x-b3-traceid");
        appHeaders.add("x-b3-spanid");
        appHeaders.add("x-b3-parentspanid");
        appHeaders.add("x-b3-sampled");
        appHeaders.add("x-b3-flags");
        appHeaders.add("x-ot-span-context");
        appHeaders.add("x-dev-user");
        appHeaders.add("fail");

        Map<String, List<String>> headersMap = Collections
                .list(request.getHeaderNames())
                .stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        h -> Collections.list(request.getHeaders(h))
                ));

        appHeaders.forEach(item -> {
            if (request.getHeader(item) != null) {
                logger.info("Header {} -> {}", item, headersMap.get(item));
                newHeaders.addAll(item, headersMap.get(item));
            }
        });
        return newHeaders;
    }

}