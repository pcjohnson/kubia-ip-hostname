package com.pcj.example.kubernetes_istio;

import org.json.JSONObject;

import java.util.Objects;

public class Response {
    private String fromAddress;
    private String serviceHostname;
    private String serviceIpAddress;
    private String serviceName;
    private String downstreamUrl;
    private JSONObject downstreamResponse;
    private String downstreamResponseStatus;
    private long timeDifferenceMS;

    public Response() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response1 = (Response) o;
        return timeDifferenceMS == response1.timeDifferenceMS &&
                Objects.equals(serviceHostname, response1.serviceHostname) &&
                Objects.equals(serviceIpAddress, response1.serviceIpAddress) &&
                Objects.equals(fromAddress, response1.fromAddress) &&
                Objects.equals(downstreamResponse, response1.downstreamResponse) &&
                Objects.equals(serviceName, response1.serviceName) &&
                Objects.equals(downstreamUrl, response1.downstreamUrl) &&
                Objects.equals(downstreamResponseStatus, response1.downstreamResponseStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serviceHostname, serviceIpAddress, fromAddress, downstreamResponse, serviceName, downstreamUrl, timeDifferenceMS, downstreamResponseStatus);
    }

    @Override
    public String toString() {
        return "{" + "\n" +
                "\"fromAddress\": \"" + fromAddress + "\",\n" +
                "\"serviceHostname\": \"" + serviceHostname + "\",\n" +
                "\"serviceIpAddress\": \"" + serviceIpAddress + "\",\n" +
                "\"serviceName\": \"" + serviceName + "\",\n" +
                "\"downstreamUrl\": \"" + downstreamUrl + "\",\n" +
                "\"downstreamResponse\": " + downstreamResponse.toString() + ",\n" +
                "\"downstreamResponseStatus\": \"" + downstreamResponseStatus + "\",\n" +
                "\"timeDifferenceMS\": \"" + timeDifferenceMS + "\"\n" +
                '}';
    }

    public String getDownstreamResponseStatus() {
        return downstreamResponseStatus;
    }

    public void setDownstreamResponseStatus(String downstreamResponseStatus) {
        this.downstreamResponseStatus = downstreamResponseStatus;
    }

    public String getDownstreamUrl() {
        return downstreamUrl;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setDownstreamUrl(String downstreamUrl) {
        this.downstreamUrl = downstreamUrl;
    }

    public String getServiceHostname() {
        return serviceHostname;
    }

    public void setServiceHostname(String serviceHostname) {
        this.serviceHostname = serviceHostname;
    }

    public String getServiceIpAddress() {
        return serviceIpAddress;
    }

    public void setServiceIpAddress(String serviceIpAddress) {
        this.serviceIpAddress = serviceIpAddress;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public JSONObject getDownstreamResponse() {
        return downstreamResponse;
    }

    public void setDownstreamResponse(JSONObject downstreamResponse) {
        this.downstreamResponse = downstreamResponse;
    }

    public long getTimeDifferenceMS() {
        return timeDifferenceMS;
    }

    public void setTimeDifferenceMS(long timeDifferenceMS) {
        this.timeDifferenceMS = timeDifferenceMS;
    }

}
