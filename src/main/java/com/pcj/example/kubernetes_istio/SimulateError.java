package com.pcj.example.kubernetes_istio;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Simulate Internal Error")
public class SimulateError extends RuntimeException {

    public SimulateError(String message) {
        super(message);
    }
}
