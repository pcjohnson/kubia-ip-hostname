package com.pcj.example.kubernetes_istio;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class GetHostDetailsTest {

    @DisplayName("Test getting the failure rate when null")
    @Test
    void testFailRateNull() {
        assertEquals(0, GetHostDetails.getFailRate(null));
    }

    @DisplayName("Test getting the failure rate when NAN")
    @Test
    void testFailRateNAN() {
        assertEquals(0, GetHostDetails.getFailRate("test"));
    }

    @DisplayName("Test getting the failure rate 0.5")
    @Test
    void testFailRate() {
        assertEquals(0.5, GetHostDetails.getFailRate("0.5"));
    }

}